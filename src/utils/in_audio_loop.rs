extern crate portaudio;

use portaudio as pa;
use std::result::Result;

use CHANNELS;
use SAMPLE_RATE;
use FRAMES;
use INTERLEAVED;

type Stream = pa::Stream<pa::NonBlocking,pa::Duplex<f32,f32>>;
type StreamResult = Result<Stream, pa::Error>;

pub fn perform<F: 'static>(f: F) -> StreamResult
    where F: Fn(&[f32], &mut [f32])
{

    // get an audio host
    let pa = try!(pa::PortAudio::new());

    // set up input
    let def_input = try!(pa.default_input_device());
    let input_info = try!(pa.device_info(def_input));
    let latency = input_info.default_low_input_latency;
    let input_params = pa::StreamParameters::<f32>::new(
        def_input,
        CHANNELS,
        INTERLEAVED,
        latency
    );

    // set up output
    let def_output = try!(pa.default_output_device());
    let output_info = try!(pa.device_info(def_output));
    let latency = output_info.default_low_output_latency;
    let output_params = pa::StreamParameters::<f32>::new(
        def_output,
        CHANNELS,
        INTERLEAVED,
        latency
    );
    
    // try to construct a duplex stream
    try!(pa.is_duplex_format_supported(
        input_params,
        output_params,
        SAMPLE_RATE
    ));

    let settings = pa::DuplexStreamSettings::new(
        input_params,
        output_params,
        SAMPLE_RATE,
        FRAMES
    );

    let callback = move |pa::DuplexStreamCallbackArgs { in_buffer, out_buffer, frames, .. }| {
        assert!(frames == FRAMES as usize);
        f(in_buffer, out_buffer);
        pa::Continue
    };

    let mut stream = try!(pa.open_non_blocking_stream(settings, callback));
    try!(stream.start());

    Ok(stream)
}
