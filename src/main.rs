extern crate portaudio;

mod ugens;
mod utils;
mod sketches;
use sketches::delay_manipulate;
use sketches::many_delays;
use sketches::fft_one;

const SAMPLE_RATE: f64 = 44_100.0;
const FRAMES: u32 = 256;
const CHANNELS: i32 = 2;
const INTERLEAVED: bool = true;

fn main() {
    // delay_manipulate::run().unwrap()
    // many_delays::run().unwrap()
    fft_one::run().unwrap()
}


