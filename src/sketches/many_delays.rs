extern crate portaudio;
extern crate rand;

use portaudio as pa;
use super::super::utils::in_audio_loop;
use super::super::ugens::delay::{Delay};

pub fn run() -> Result<(), pa::Error> {
    // use a channel to send count_down value to main thread
    // cuz we're 1337
    // let (sender, receiver) = ::std::sync::mpsc::channel();
    use std::sync::{Arc, Mutex};
    use self::rand::Rng;

    let mut generator = rand::thread_rng();
    let num_delays = (generator.gen::<f64>() * 100.0).round() as u32;
    println!("using {} delays.", num_delays);

    let delays = (0..num_delays).map(|_| {
        Delay::new(generator.gen::<f64>() + 0.01, 1.0)
    }).collect::<Vec<Delay>>();
    let delays = Arc::new(Mutex::new(delays));

    let callback_delays = delays.clone();
    let stream = try!(in_audio_loop::perform(move |in_buffer, out_buffer| {
        let volume_mod = 0.9 / num_delays as f32;

        let res = callback_delays.try_lock()
            .map(|mut delays| {
                for (input_sample, output_sample) in in_buffer.iter().zip(out_buffer.iter_mut()) {
                    let mut output = 0.0;
                    for delay in delays.iter_mut() {
                        output += delay.call(*input_sample) * volume_mod;
                    }
                    *output_sample = output;
                }
            });

        match res {
            Ok(_) => (),
            Err(e) => println!("{:?}", e)
        };
    }));

    while try!(stream.is_active()) {}
    Ok(())
}
