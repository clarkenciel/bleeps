extern crate rustfft;
extern crate portaudio;
extern crate ordered_float;

use std::{thread,time};
use std::sync::{Arc,Mutex};
use std::sync::mpsc;
use std::sync::mpsc::{Sender,Receiver};
use portaudio as pa;
use super::super::utils::in_audio_loop;
use self::rustfft::FFTplanner;
use self::rustfft::num_complex::Complex;
use self::rustfft::num_traits::Zero;
use self::ordered_float::OrderedFloat;

pub fn run() -> Result<(), pa::Error> {

    let (sender, receiver): (Sender<Vec<f32>>, Receiver<Vec<f32>>) = mpsc::channel();
    let stream = try!(in_audio_loop::perform(move |in_buf, _| {
        let in_vec = in_buf.iter().map(|s| *s).collect::<Vec<f32>>();
        match sender.send(in_vec) {
            Ok(_) => (),
            Err(e) => println!("{}", e)
        }
    }));

    thread::spawn(move || {
        let mut input: Vec<Complex<f32>> = vec![];
        let mut result: Vec<Complex<f32>> = vec![];
        let mut planner = FFTplanner::new(false);
        loop {
            receiver.recv().map(|samples| {
                input = samples.iter().map(|s| Complex::from(s)).collect();
                result = input.iter().map(|_| Complex::zero()).collect();
                let fft = planner.plan_fft(512);
                fft.process(input.as_mut_slice(), &mut result);

                let loudest = result.iter()
                    .map(|c| OrderedFloat(c.re))
                    .enumerate()
                    .max_by_key(|p| p.1)
                    .map(|max| max.0);

                match loudest {
                    Some(x) => println!("maximum bin: {}", x),
                    None => println!("no max found")
                }
            }).unwrap_or_else(|e| { println!("{}", e) });
        }
    });

    while try!(stream.is_active()) {}

    Ok(())
}
