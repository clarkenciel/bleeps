extern crate portaudio;
extern crate rand;

use portaudio as pa;
use super::super::utils::in_audio_loop;
use super::super::ugens::delay::{Delay};

pub fn run() -> Result<(), pa::Error> {
    // use a channel to send count_down value to main thread
    // cuz we're 1337
    // let (sender, receiver) = ::std::sync::mpsc::channel();
    use std::sync::{Arc, Mutex};
    let delay = Arc::new(Mutex::new(Delay::new(0.75, 1.0)));

    let callback_delay = delay.clone();
    let stream = try!(in_audio_loop::perform(move |in_buffer, out_buffer| {
        let res = callback_delay.try_lock()
            .map(|mut delay| {
                for (input_sample, output_sample) in in_buffer.iter().zip(out_buffer.iter_mut()) {
                    *output_sample = *input_sample;
                    *output_sample = delay.call(*output_sample);
                }
            });

        match res {
            Ok(_) => (),
            Err(e) => println!("{:?}", e)
        };
    }));

    use std::{thread, time};
    use self::rand::Rng;
    
    let mut generator = rand::thread_rng();
    let mut new_delay: f64;
    let main_delay = delay.clone();
    while try!(stream.is_active()) {
        thread::sleep(time::Duration::from_millis(1000));
        new_delay = 0.1 + generator.gen::<f64>() * 0.75;
        println!("setting delay to: {}", new_delay);

        let res = main_delay.try_lock().map(|mut delay| {
            delay.set_delay(new_delay).map_err(|e| { println!("{}", e); })
        });

        match res {
            Err(e) => println!("{}", e),
            Ok(_) => ()
        };
    };

    Ok(())
}
