use SAMPLE_RATE;

pub struct Delay {
    delay_length: u64,
    max_delay_length: u64,
    read_head: usize,
    write_head: usize,
    buffer: Vec<f32>,
    delay_counter: u64
}

impl Delay {
    pub fn new(dl: f64, mdl: f64) -> Delay {
        let delay_length = (dl * SAMPLE_RATE).ceil() as u64;
        let max_length = (mdl * SAMPLE_RATE).ceil() as u64;
        let buffer_size = max_length as usize;
        
        Delay {
            delay_length: delay_length,
            max_delay_length: max_length,
            read_head: 0,
            write_head: 0,
            buffer: (0..buffer_size).map(|_| { 0.0 }).collect(),
            delay_counter: 0
        }
    }

    pub fn call(&mut self, input: f32) -> f32 {
        self.buffer[self.write_head] = input;
        self.write_head = (self.write_head + 1) % self.max_delay_length as usize;

        let mut output = input * 0.5;
        if self.delay_counter > self.delay_length {
            output += self.buffer[self.read_head] * 0.5;
            self.read_head = (self.read_head + 1) % self.max_delay_length as usize;
        } else {
            self.delay_counter += 1;
        }

        output
    }

    pub fn set_delay(&mut self, delay: f64) -> Result<(), &'static str> {
        let max = self.max_delay_length as f64 / SAMPLE_RATE;
        if max < delay {
            Err("Delay length must be less than max delay length")
        } else {
            self.delay_length = (delay * SAMPLE_RATE).ceil() as u64;
            self.delay_counter = 0;
            self.read_head = self.write_head;
            Ok(())
        }
    }
}
